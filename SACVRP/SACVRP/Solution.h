// ***********************************************************************
// Assembly         : 
// Author           : Wira Redi
// Created          : 04-01-2015
//
// Last Modified By : Wira Redi
// Last Modified On : 04-01-2015
// ***********************************************************************
// <copyright file="Solution.h" company="Global Supply Chain Management NTUST">
//     Copyright (c) Global Supply Chain Management NTUST. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
#pragma once
#ifndef _SOLUTIONSET_
#define _SOLUTIONSET_

#include "Parameter.h"

/// <summary>
/// Class Solution.
/// </summary>
class Solution
{
public:
	/// <summary>
	/// The route
	/// </summary>
	int Route[MAX_CODINGLENGTH];
	/// <summary>
	/// The length
	/// </summary>
	int Length;
	/// <summary>
	/// The feasible
	/// </summary>
	int Feasible;
	/// <summary>
	/// The objective
	/// </summary>
	float Objective;
	/// <summary>
	/// The penalty
	/// </summary>
	float Penalty;
	Solution(void);
	~Solution(void);
	void CopySolution(Solution*);
};

#endif