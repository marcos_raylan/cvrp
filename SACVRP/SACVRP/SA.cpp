// ***********************************************************************
// Assembly         : 
// Author           : Wira Redi
// Created          : 04-01-2015
//
// Last Modified By : Wira Redi
// Last Modified On : 04-01-2015
// ***********************************************************************
// <copyright file="SA.cpp" company="Global Supply Chain Management NTUST">
//     Copyright (c) Global Supply Chain Management NTUST. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
#include "SA.h"

/// <summary>
/// Initializes a new instance of the <see cref="SA" /> class.
/// </summary>
/// / The initial temporary
SA::SA()
{
	this->initialsolution = new InitialSolution();
	this->neighborhood = new Neighborhood();
}
/// <summary>
/// Finalizes an instance of the <see cref="SA" /> class.
/// </summary>
SA::~SA(){}

/// <summary>
/// The initial temporary
/// </summary>
void SA::Start()
{
	this->Temperature = InitialTemp;
	this->NonImproveCount = 0;
	float boltzman,r;
	float delta;
	clock_t start;
	srand(seed);
	start=clock();
	this->InitializeSolution();
	problem->CurrentSolution->CopySolution(problem->BestSolution);
	while(this->Temperature > this->maxTemp && this->NonImproveCount < this->nonImprove)
	{
		if(NonImproveCount % 1000 == 0)
			this->problem->CurrentSolution->CopySolution(this->problem->BestSolution);
		for(int j=0;j<this->iteration;j++)
		{
			this->problem->WorkingSolution->CopySolution(this->problem->CurrentSolution);

			r = getSRand();
			if(r >= 0.667f)
				this->neighborhood->Swap(this->problem->WorkingSolution);
			else if(r >= 0.337f)
				this->neighborhood->Insert(this->problem->WorkingSolution);
			else
				this->neighborhood->Reverse(this->problem->WorkingSolution);

			this->problem->CalculateObjective(this->problem->WorkingSolution);
			delta = this->problem->WorkingSolution->Objective - this->problem->CurrentSolution->Objective;
			boltzman = (float)exp(-delta/Temperature);
			if(delta<=0)
				this->problem->CurrentSolution->CopySolution(this->problem->WorkingSolution);
			else
			{
				r = getSRand();
				if (r < boltzman)
					this->problem->CurrentSolution->CopySolution(this->problem->WorkingSolution);
			}

			if (this->problem->CurrentSolution->Objective < this->problem->BestSolution->Objective)
			{
 				this->problem->BestSolution->CopySolution(this->problem->CurrentSolution);
				this->NonImproveCount = 0;
				this->Temperature += this->Temperature*0.15;
				//printf("%f %f\n",Temperature,this->problem->BestSolution->Objective);
			}

			NonImproveCount++;
		}

		
		//printf("%f %f\n",Temperature,this->problem->BestSolution->Objective);

		this->Temperature *= this->alpha;
	}
	this->ComputationTime = (clock()-start)/(double)CLOCKS_PER_SEC;
}