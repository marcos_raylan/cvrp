// ***********************************************************************
// Assembly         : 
// Author           : Wira Redi
// Created          : 04-01-2015
//
// Last Modified By : Wira Redi
// Last Modified On : 04-01-2015
// ***********************************************************************
// <copyright file="Problem.h" company="Global Supply Chain Management NTUST">
//     Copyright (c) Global Supply Chain Management NTUST. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
#include "Solution.h"
#include "Node.h"
#include "Parameter.h"
#include "ResultStream.h"

#pragma once
#ifndef _PROBLEM_
#define _PROBLEM_

/// <summary>
/// Class Problem.
/// </summary>
class Problem
{
public:
	/// <summary>
	/// The n customer
	/// </summary>
	int NCustomer;

	/// <summary>
	/// The n dummy
	/// </summary>
	int NDummy;

	/// <summary>
	/// The coding length
	/// </summary>
	int CodingLength;

	/// <summary>
	/// The type 
	/// type = : {1 = Christofides 1979, 2 = Augerat ... }
	/// </summary>
	int Type;

	/// <summary>
	/// The total demand
	/// </summary>
	float TotalDemand;

	/// <summary>
	/// The capacity
	/// </summary>
	float Capacity;

	/// <summary>
	/// The maximum route time
	/// </summary>
	float MaxRouteTime;

	/// <summary>
	/// The service time
	/// </summary>
	float ServiceTime;

	/// <summary>
	/// The xmax
	/// </summary>
	float Xmax;

	/// <summary>
	/// The ymax
	/// </summary>
	float Ymax;

	/// <summary>
	/// The penalty
	/// </summary>
	float penalty;

	/// <summary>
	/// The rmax
	/// </summary>
	float Rmax;
	
	/// <summary>
	/// The best solution
	/// </summary>
	Solution* BestSolution;
	
	/// <summary>
	/// The current solution
	/// </summary>
	Solution* CurrentSolution;
	
	/// <summary>
	/// The working solution
	/// </summary>
	Solution* WorkingSolution;
	
	/// <summary>
	/// The out
	/// </summary>
	ofstream Out;
	
	/// <summary>
	/// The log
	/// </summary>
	ofstream Log;
	
	/// <summary>
	/// The debug
	/// </summary>
	ofstream Debug;
	
	/// <summary>
	/// The nodes
	/// </summary>
	Node* Nodes[MAX_CODINGLENGTH];
	
	/// <summary>
	/// The distance matrix
	/// </summary>
	float DistanceMatrix[MAX_CODINGLENGTH][MAX_CODINGLENGTH];
	Problem();
	~Problem();
	virtual void initiateOutput(string);
	virtual void CalculateDistanceMatrix();
	virtual float CalculateObjective(Solution*);
	virtual float CalculateObjeciveAugerat(Solution*);
	virtual float CalculateObjeciveCMT(Solution*);
	virtual void ReadProblem(string);
	virtual void ReadProblemCMT(string);
	virtual void ReadProblemAugerat(string);
	virtual void PrintSolution(Solution*);
	virtual void PrintSolutionAugerat(Solution*);
	virtual void PrintSolutionCMT(Solution*);
	virtual Solution* ReadSolution(string);
};

#endif