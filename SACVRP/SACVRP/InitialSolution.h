// ***********************************************************************
// Assembly         : 
// Author           : Wira Redi
// Created          : 04-01-2015
//
// Last Modified By : Wira Redi
// Last Modified On : 03-30-2015
// ***********************************************************************
// <copyright file="InitialSolution.h" company="Global Supply Chain Management NTUST">
//     Copyright (c) Global Supply Chain Management NTUST. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
#include "Problem.h"
#pragma once
#ifndef _INITIALSOLUTION_
#define _INITIALSOLUTION_

/// <summary>
/// Class InitialSolution.
/// </summary>
class InitialSolution
{
public:
	/// <summary>
	/// The problem
	/// </summary>
	Problem* problem;
	Solution* result;
	Solution* tempResult;
	InitialSolution();
	~InitialSolution();
	Solution* NearestNeighbour();
};
#endif