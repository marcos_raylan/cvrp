#include "Neighborhood.h"

/// <summary>
/// Initializes a new instance of the <see cref="Neighborhood"/> class.
/// </summary>
/// / The problem
Neighborhood::Neighborhood()
{
	this->problem = new Problem();
}
/// <summary>
/// Finalizes an instance of the <see cref="Neighborhood"/> class.
/// </summary>
/// <summary>
/// The problem
/// </summary>
/// <param name="">The .</param>
void Neighborhood::Swap(Solution* sol)
{
	int point1;
	do {
		point1 = rand() % sol->Length;
	} while (point1 >= sol->Length || point1 == 0);

	int point2;
	do {
		point2 = rand() % sol->Length;
	} while (point1==point2 || point2 >= sol->Length || point2 == 0);

	int temp;
	temp = sol->Route[point1];
	sol->Route[point1] = sol->Route[point2];
	sol->Route[point2] = temp;
}

/// <summary>
/// Inserts the specified .
/// </summary>
/// <param name="">The .</param>
void Neighborhood::Insert(Solution* sol)
{
	int x, y, jobNo, intCount;
	int tempSeq[MAX_CODINGLENGTH];

	do 
	{
		x = getRand(sol->Length);
		if (x >= sol->Length) x = sol->Length - 1;
	} while (x == 0);

	do 
	{
		y = getRand(sol->Length);
		if (y >= sol->Length) y = sol->Length - 1;
	} while (y == x || y == 0);

	jobNo = sol->Route[x];
	intCount = 0;
	for (int i = 0; i < x;i++)
	{ 
		tempSeq[intCount] = sol->Route[i];
		intCount++;
	}
	for (int i = x + 1; i < sol->Length; i++)
	{ 
		tempSeq[intCount] = sol->Route[i];
		intCount++;
	}
	for (int k = 0; k < y; k++) 
		sol->Route[k] = tempSeq[k];
	
	sol->Route[y] = jobNo;
	for (int k = y; k < sol->Length-1; k++) 
		sol->Route[k+1] = tempSeq[k];
}

/// <summary>
/// Reverses the specified .
/// </summary>
/// <param name="">The .</param>
void Neighborhood::Reverse(Solution* sol)
{
	int x, y, jobNo, intCount,temp;
	int tempSeq[MAX_CODINGLENGTH];

	do 
	{
		x = getRand(sol->Length);
		if (x >= sol->Length) x = sol->Length - 1;
	} while (x == 0);

	do 
	{
		y = getRand(sol->Length);
		if (y >= sol->Length) y = sol->Length - 1;
	} while (y == x || y == 0);

	if(x > y)
	{
		temp = x;
		x = y;
		y = temp;
	}

	for(int i=x;i<=y;i++)
	{
		tempSeq[i] = sol->Route[i];
	}
	
	int j=x;
	for(int i=y;i>=x;i--)
	{
		sol->Route[j] = tempSeq[i];
		j++;
	}
}

/// <summary>
/// Quicks the sort.
/// </summary>
/// <param name="">The .</param>
/// <param name="">The .</param>
/// <param name="">The .</param>
/// <param name="">The .</param>
void Neighborhood::QuickSort(double value[],int sequence[],int left,int right)
{
 	int k,j;
	double pivot;

	if(left<right)
	{
		pivot=value[(left+right)/2];
		k=left-1;
		j=right+1;
	
		while(1)
		{
			while(value[++k]<pivot);
			while(value[--j]>pivot);
			
			if(k>=j)
				break;

			std::swap(value[k],value[j]);
			std::swap(sequence[k],sequence[j]);
		}

		QuickSort(value,sequence,left,k-1);
		QuickSort(value,sequence,j+1,right);

	}
}  