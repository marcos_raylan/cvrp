// ***********************************************************************
// Assembly         : 
// Author           : Wira Redi
// Created          : 04-01-2015
//
// Last Modified By : Wira Redi
// Last Modified On : 04-01-2015
// ***********************************************************************
// <copyright file="Main.cpp" company="Global Supply Chain Management NTUST">
//     Copyright (c) Global Supply Chain Management NTUST. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
#include "SA.h"

/// <summary>
/// The problem
/// </summary>
Problem* problem = new Problem();
/// <summary>
/// The solver
/// </summary>
SA* solver = new SA();
/// <summary>
/// The input, Type = 2
/// </summary>
//string input[100] = { "./Augerat/A-n32-k5.vrp", "./Augerat/A-n33-k5.vrp", "./Augerat/A-n33-k6.vrp", "./Augerat/A-n34-k5.vrp", "./Augerat/A-n36-k5.vrp",
//"./Augerat/A-n37-k5.vrp", "./Augerat/A-n37-k6.vrp", "./Augerat/A-n38-k5.vrp", "./Augerat/A-n39-k5.vrp", "./Augerat/A-n39-k6.vrp",
//"./Augerat/A-n44-k7.vrp", "./Augerat/A-n45-k6.vrp", "./Augerat/A-n45-k7.vrp", "./Augerat/A-n46-k7.vrp", "./Augerat/A-n48-k7.vrp",
//"./Augerat/A-n53-k7.vrp", "./Augerat/A-n54-k7.vrp", "./Augerat/A-n55-k9.vrp", "./Augerat/A-n60-k9.vrp", "./Augerat/A-n61-k9.vrp",
//"./Augerat/A-n62-k8.vrp", "./Augerat/A-n63-k9.vrp", "./Augerat/A-n63-k10.vrp", "./Augerat/A-n64-k9.vrp", "./Augerat/A-n65-k9.vrp",
//"./Augerat/A-n69-k9.vrp", "./Augerat/A-n80-k10.vrp" };

/// <summary>
/// The input , Type = 1
/// </summary>
string input[100] = { "./vrp/vrpnc1.txt","./vrp/vrpnc2.txt","./vrp/vrpnc3.txt","./vrp/vrpnc4.txt","./vrp/vrpnc5.txt","./vrp/vrpnc6.txt","./vrp/vrpnc7.txt","./vrp/vrpnc8.txt"
, "./vrp/vrpnc9.txt","./vrp/vrpnc10.txt","./vrp/vrpnc11.txt","./vrp/vrpnc12.txt","./vrp/vrpnc13.txt","./vrp/vrpnc14.txt" };

ofstream Summary;

void PrintResult();

/// <summary>
/// Mains the specified argc.
/// </summary>
/// <param name="argc">The argc.</param>
/// <param name="argv">The argv.</param>
/// <returns>int.</returns>
int main(int argc, char* argv[])
{
	int i = 14;
	Summary.open("./vrp/summary.txt");
	for (int j = 0; j < i; j++)
	{
	problem->Type = 1;
	problem->ReadProblem(input[j]);
	solver->nonImprove = NNonImprove; solver->maxTemp = MaxTemp; solver->alpha = Alpha; solver->initialTemp = InitialTemp; solver->iteration = num_generation;
	solver->randseed = 0; solver->seed = 1;
	solver->InitiateProblem(problem);
	solver->Start();
	PrintResult();
	problem->Out << endl;
	problem->Out.close();
	problem->Log.close();
	problem->Debug.close();
	}
	Summary.close();
	system("PAUSE");
	return 0;	
}

void PrintResult()
{
	// PRINT RESULT
	problem->PrintSolution(solver->problem->BestSolution);
	cout << "final solution : " << solver->problem->BestSolution->Objective << "\n";
	cout << "time(s) : " << solver->ComputationTime << "\n";
	problem->Out << "\n";
	problem->Out << "Objective : " << solver->problem->BestSolution->Objective << "\n time(s) : " << solver->ComputationTime << "\n";
	Summary << solver->problem->BestSolution->Objective << "\t" << solver->ComputationTime << "\n";
	problem->Out << "Route : \n";
	for (int i = 0; i<problem->NCustomer; i++)
	{
		problem->Out << solver->problem->BestSolution->Route[i] << " ";
		if (i % 20 == 0 && i != 0) problem->Out << endl;
	}
	// PRINT CONFIGURATION
	problem->Out << "\n CONFIGURATION " << "\n";
	problem->Out << "Iteration " << solver->iteration << "\n";
	problem->Out << "Non Improve " << solver->nonImprove << "\n";
	problem->Out << "alpha " << solver->alpha << "\n";
	problem->Out << "Initial Temp " << solver->initialTemp << "\n";
	problem->Out << "Max Temp " << solver->maxTemp << "\n";
	problem->Out << "SEED " << solver->seed << "\n";

}