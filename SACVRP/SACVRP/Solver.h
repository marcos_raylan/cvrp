// ***********************************************************************
// Assembly         : 
// Author           : Wira Redi
// Created          : 04-01-2015
//
// Last Modified By : Wira Redi
// Last Modified On : 04-01-2015
// ***********************************************************************
// <copyright file="Solver.h" company="Global Supply Chain Management NTUST">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
#include "Problem.h"
#include "InitialSolution.h"
#include "Neighborhood.h"
#include "ResultStream.h"

#pragma once
#ifndef _SOLVER_
#define _SOLVER_

/// <summary>
/// Class Solver.
/// </summary>
class Solver
{
public:
	/// <summary>
	/// The non improve count
	/// </summary>
	int NonImproveCount;
	/// <summary>
	/// The seed
	/// </summary>
	int seed;
	/// <summary>
	/// The seed number
	/// </summary>
	int seedNumber;
	/// <summary>
	/// The randseed
	/// </summary>
	int randseed;
	/// <summary>
	/// The computation time
	/// </summary>
	double ComputationTime;
	/// <summary>
	/// The problem
	/// </summary>
	Problem* problem;
	/// <summary>
	/// The neighborhood
	/// </summary>
	Neighborhood* neighborhood;
	/// <summary>
	/// The initialsolution
	/// </summary>
	InitialSolution* initialsolution;
	Solver();
	~Solver();
	virtual void Start();
	virtual void InitializeSolution();
	virtual void InitiateProblem(Problem*);
};
#endif