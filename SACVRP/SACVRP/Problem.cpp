// ***********************************************************************
// Assembly         : 
// Author           : Wira Redi
// Created          : 04-01-2015
//
// Last Modified By : Wira Redi
// Last Modified On : 04-01-2015
// ***********************************************************************
// <copyright file="Problem.cpp" company="Global Supply Chain Management NTUST">
//     Copyright (c) Global Supply Chain Management NTUST. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
#include "Problem.h"

/// <summary>
/// Initializes a new instance of the <see cref="Problem" /> class.
/// </summary>
Problem::Problem()
{ 
	this->NCustomer=0;
	this->CurrentSolution = new Solution();
	this->WorkingSolution = new Solution();
	this->BestSolution = new Solution();
}
/// <summary>
/// Finalizes an instance of the <see cref="Problem" /> class.
/// </summary>
/// / The capacity
Problem::~Problem(){}

/// <summary>
/// The capacity
/// </summary>
void Problem::CalculateDistanceMatrix()
{
	for(int i = 0; i < this->NCustomer; i++)
		for(int j = 0; j < this->NCustomer; j++)
		{
			if (this->Type == 2) { this->DistanceMatrix[i][j] = round(sqrt(pow(this->Nodes[i]->X - this->Nodes[j]->X, 2) + pow(this->Nodes[i]->Y - this->Nodes[j]->Y, 2))); }
			else if (this->Type == 1) { this->DistanceMatrix[i][j] = sqrt(pow(this->Nodes[i]->X - this->Nodes[j]->X, 2) + pow(this->Nodes[i]->Y - this->Nodes[j]->Y, 2)); }
		}
}

/// <summary>
/// Calculates the objective.
/// </summary>
/// <param name="sol">The sol.</param>
/// <returns>float.</returns>
float Problem::CalculateObjective(Solution* sol)
{
	try{
		if (this->Type == 1)
		{
			return this->CalculateObjeciveCMT(sol);
		}
		else if (this->Type == 2)
		{
			return this->CalculateObjeciveAugerat(sol);
		}
		else
		{
			throw 1;
		}
	}
	catch (int e)
	{
		if (e == 1)
		 cout << "An exception occurred in CalculateObjective. Exception : " << "Problem type is not selected" << '\n';
	}
}

/// <summary>
/// Initiates the output.
/// </summary>
/// <param name="input">The input.</param>
void Problem::initiateOutput(string input)
{
	string temp = input + ".out";
	Out.open(temp.c_str(),ios::out);
	temp = input + string(".log");
	Log.open(temp.c_str(),ios::out);
	temp = input + ".debug"; 
	Debug.open(temp.c_str(),ios::out);
}

/// <summary>
/// Reads the problem.
/// </summary>
/// <param name="input">The input.</param>
void Problem::ReadProblem(string input)
{
	initiateOutput(input);
	try{
		if (this->Type == 1)
		{
			this->ReadProblemCMT(input);
		}
		else if (this->Type == 2)
		{
			this->ReadProblemAugerat(input);
		}
		else
		{
			throw 1;
		}
	}
	catch (int e)
	{
		if (e == 1)
			cout << "An exception occurred in ReadProblem. Exception : " << "Problem type is not selected" << '\n';
	}
}

/// <summary>
/// Reads the problem augerat.
/// </summary>
/// <param name="input">The input.</param>
void Problem::ReadProblemAugerat(string input)
{
	float totalDemand = 0;
	FILE* infile;
	fopen_s(&infile, input.c_str(), "r");
	if (infile == NULL)
	{
		Log << "Can't find this file" << endl;
		exit(0);
	}
	fscanf_s(infile, "%d %f", &this->NCustomer, &this->Capacity);
	for (int i = 0; i < this->NCustomer; i++)
	{
		this->Nodes[i] = new Node();
		fscanf_s(infile, "%d %f %f", &Nodes[i]->ID, &Nodes[i]->X, &Nodes[i]->Y);
	}

	for (int i = 0; i < this->NCustomer; i++)
	{
		fscanf_s(infile, "%d %f", &Nodes[i]->ID, &Nodes[i]->Demand);
		totalDemand += Nodes[i]->Demand;
	}

	this->NDummy = (int)ceil(totalDemand / this->Capacity);

	fclose(infile);
	this->CalculateDistanceMatrix();
}

/// <summary>
/// Reads the problem CMT.
/// </summary>
/// <param name="input">The input.</param>
void Problem::ReadProblemCMT(string input)
{
	// Get boundary of Map
	this->Xmax = 0; this->Ymax = 0;

	this->TotalDemand = 0;
	FILE* infile;
	fopen_s(&infile, input.c_str(), "r");
	if (infile == NULL)
	{
		Log << "Can't find this file" << endl;
		exit(0);
	}

	// Read number of customers, vehicle capacity, maximum route time, drop time
	fscanf_s(infile, "%d %f %f %f", &this->NCustomer, &this->Capacity, &this->MaxRouteTime, &this->ServiceTime);
	this->NCustomer++;

	// Read : depot x-coordinate, depot y-coordinate 
	this->Nodes[0] = new Node(); this->Nodes[0]->ID = 0; this->Nodes[0]->Demand = 0;
	fscanf_s(infile, "%f %f", &Nodes[0]->X, &Nodes[0]->Y);
	// initial boundary
	Xmax = Nodes[0]->X; Ymax = Nodes[0]->Y;

	// Read : for each customer in turn: x-coordinate, y-coordinate, quantity
	for (int i = 1; i < this->NCustomer; i++)
	{
		this->Nodes[i] = new Node();
		fscanf_s(infile, "%f %f %f", &Nodes[i]->X, &Nodes[i]->Y, &Nodes[i]->Demand);
		Nodes[i]->ID = i;
		TotalDemand += Nodes[i]->Demand;
		// initial boundary
		if (Nodes[i]->X > Xmax) Xmax = Nodes[i]->X; if (Nodes[i]->Y > Ymax) Ymax = Nodes[i]->Y;
	}

	if (Xmax > Ymax) Rmax = Xmax; else Rmax = Ymax;

	// Dummy zero
	this->NDummy = (int)ceil(TotalDemand / this->Capacity) + 1;
	this->CodingLength = this->NCustomer + this->NDummy;
	this->penalty = 10;

	fclose(infile);
	this->CalculateDistanceMatrix();
}

/// <summary>
/// Calculates the objecive augerat.
/// </summary>
/// <param name="sol">The sol.</param>
/// <returns>float.</returns>
float Problem::CalculateObjeciveAugerat(Solution* sol)
{
	float obj = 0;
	float currentCapacity = 0;
	int from, to;
	int condition = 1;
	sol->Route[sol->Length] = 0;

	for (int i = 1; i <= sol->Length; i++)
	{
		from = sol->Route[i - 1];
		to = sol->Route[i];
		currentCapacity += this->Nodes[to]->Demand;

		if (to == 0)
		{
			currentCapacity = 0;
			obj += this->DistanceMatrix[from][to];
		}
		else if (currentCapacity > this->Capacity)
		{
			obj += this->DistanceMatrix[from][0];
			currentCapacity = 0;
			obj += this->DistanceMatrix[0][to];
			currentCapacity = this->Nodes[to]->Demand;
		}
		else
		{
			obj += this->DistanceMatrix[from][to];
		}
	}
	sol->Objective = obj;
	return obj;
}

/// <summary>
/// Calculates the objecive CMT.
/// </summary>
/// <param name="sol">The sol.</param>
/// <returns>float.</returns>
float Problem::CalculateObjeciveCMT(Solution* sol)
{
	// for penalty calculation
	float capacityPenalty = 0;
	float timePenalty = 0;

	float obj = 0;
	float currentCapacity = 0;
	float currentTime = 0;
	int from, to;
	int condition = 1;
	sol->Route[sol->Length] = 0;


	for (int i = 1; i <= sol->Length; i++)
	{
		from = sol->Route[i - 1];
		to = sol->Route[i];
		currentCapacity += this->Nodes[to]->Demand;

		if (from != 0)
			currentTime += this->DistanceMatrix[from][to] + this->ServiceTime;
		else
			currentTime += this->DistanceMatrix[from][to];

		if (to == 0)
		{
			if (currentTime > this->MaxRouteTime)
			{
				timePenalty += currentTime - this->MaxRouteTime;
			}

			if (currentCapacity > this->Capacity)
			{
				capacityPenalty += currentCapacity - this->Capacity;
			}
			obj += this->DistanceMatrix[from][to];
			currentTime = 0;
			currentCapacity = 0;
		}
		else if (currentTime > this->MaxRouteTime || currentCapacity > this->Capacity)
		{
			// calculate arrival time at depot
			currentTime = currentTime - this->DistanceMatrix[from][to] + this->DistanceMatrix[from][0] + this->ServiceTime;
			currentCapacity -= this->Nodes[to]->Demand;

			if (currentTime > this->MaxRouteTime)
			{
				timePenalty += currentTime - this->MaxRouteTime;
			}

			if (currentCapacity > this->Capacity)
			{
				capacityPenalty += currentCapacity - this->Capacity;
			}

			obj += this->DistanceMatrix[from][0];
			currentCapacity = 0;
			currentTime = 0;

			// depat from depot to next node
			obj += this->DistanceMatrix[0][to];
			currentCapacity = this->Nodes[to]->Demand;
			currentTime = this->DistanceMatrix[0][to];
		}
		else
		{
			obj += this->DistanceMatrix[from][to];
		}
	}

	sol->Penalty = this->penalty*(capacityPenalty + timePenalty);
	sol->Objective = obj + sol->Penalty;
	return obj;
}

/// <summary>
/// Prints the solution.
/// </summary>
/// <param name="sol">The sol.</param>
void Problem::PrintSolution(Solution* sol)
{
	try{
		if (this->Type == 1)
		{
			this->PrintSolutionCMT(sol);
		}
		else if (this->Type == 2)
		{
			this->PrintSolutionAugerat(sol);
		}
		else
		{
			throw 1;
		}
	}
	catch (int e)
	{
		if (e == 1)
			cout << "An exception occurred in CalculateObjective. Exception : " << "Problem type is not selected" << '\n';
	}
}

/// <summary>
/// Calculates the objecive augerat.
/// </summary>
/// <param name="sol">The sol.</param>
/// <returns>float.</returns>
void Problem::PrintSolutionAugerat(Solution* sol)
{
	float obj = 0;
	float currentCapacity = 0;
	int from, to;
	int condition = 1;
	sol->Route[sol->Length] = 0;

	for (int i = 1; i <= sol->Length; i++)
	{
		from = sol->Route[i - 1];
		to = sol->Route[i];
		currentCapacity += this->Nodes[to]->Demand;

		if (to == 0)
		{
			currentCapacity = 0;
			obj += this->DistanceMatrix[from][to];
		}
		else if (currentCapacity > this->Capacity)
		{
			obj += this->DistanceMatrix[from][0];
			currentCapacity = 0;
			obj += this->DistanceMatrix[0][to];
			currentCapacity = this->Nodes[to]->Demand;
		}
		else
		{
			obj += this->DistanceMatrix[from][to];
		}
	}
	sol->Objective = obj;
}

/// <summary>
/// Calculates the objecive CMT.
/// </summary>
/// <param name="sol">The sol.</param>
/// <returns>float.</returns>
void Problem::PrintSolutionCMT(Solution* sol)
{
	// for penalty calculation
	float capacityPenalty = 0;
	float timePenalty = 0;

	float obj = 0;
	float currentCapacity = 0;
	float currentTime = 0;
	int from, to;
	int condition = 1;
	sol->Route[sol->Length] = 0;
	this->Debug << "from" << "\t" << "to" << "\t" << "distance" << "\t" << "currentTime" << "\t" << "currentCapacity" << "\t" << "objective" << endl;

	for (int i = 1; i <= sol->Length; i++)
	{
		from = sol->Route[i - 1];
		to = sol->Route[i];
		currentCapacity += this->Nodes[to]->Demand;

		if (from != 0)
			currentTime += this->DistanceMatrix[from][to] + this->ServiceTime;
		else
			currentTime += this->DistanceMatrix[from][to];

		if (to == 0)
		{
			if (currentTime > this->MaxRouteTime)
			{
				timePenalty += currentTime - this->MaxRouteTime;
			}

			if (currentCapacity > this->Capacity)
			{
				capacityPenalty += currentCapacity - this->Capacity;
			}
			obj += this->DistanceMatrix[from][to];
			this->Debug << from << "\t" << to << "\t" << this->DistanceMatrix[from][to] << "\t" << currentTime << "\t" << currentCapacity << "\t" << obj << endl;
			currentTime = 0;
			currentCapacity = 0;
		}
		else if (currentTime > this->MaxRouteTime || currentCapacity > this->Capacity)
		{
			// calculate arrival time at depot
			currentTime = currentTime - this->DistanceMatrix[from][to] + this->DistanceMatrix[from][0] + this->ServiceTime;
			currentCapacity -= this->Nodes[to]->Demand;

			if (currentTime > this->MaxRouteTime)
			{
				timePenalty += currentTime - this->MaxRouteTime;
			}

			if (currentCapacity > this->Capacity)
			{
				capacityPenalty += currentCapacity - this->Capacity;
			}

			obj += this->DistanceMatrix[from][0];

			this->Debug << from << "\t" << "0" << "\t" << this->DistanceMatrix[from][0] << "\t" << currentTime << "\t" << currentCapacity << "\t" << obj << endl;

			currentCapacity = 0;
			currentTime = 0;

			// depat from depot to next node
			obj += this->DistanceMatrix[0][to];
			currentCapacity = this->Nodes[to]->Demand;
			currentTime = this->DistanceMatrix[0][to];

			this->Debug << "0" << "\t" << to << "\t" << this->DistanceMatrix[0][to] << "\t" << currentTime << "\t" << currentCapacity << "\t" << obj << endl;
		}
		else
		{
			obj += this->DistanceMatrix[from][to];
			this->Debug << from << "\t" << to << "\t" << this->DistanceMatrix[from][to] << "\t" << currentTime << "\t" << currentCapacity << "\t" << obj << endl;
		}
	}

	sol->Penalty = this->penalty*(capacityPenalty + timePenalty);
	sol->Objective = obj + sol->Penalty;
}

Solution* Problem::ReadSolution(string input)
{
	Solution* sol = new Solution();
	FILE* infile;
	fopen_s(&infile, input.c_str(), "r");
	if (infile == NULL)
	{
		Log << "Can't find this file" << endl;
		exit(0);
	}

	int nVehicle;
	string line;

	fscanf_s(infile, "%d", &nVehicle);
	for (int i = 0; i < nVehicle; i++)
	{
		fscanf_s(infile, "%d", &nVehicle);
	}

}
