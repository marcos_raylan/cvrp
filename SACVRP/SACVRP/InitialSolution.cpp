#include "InitialSolution.h"

/// <summary>
/// Initializes a new instance of the <see cref="InitialSolution"/> class.
/// </summary>
/// / The problem
InitialSolution::InitialSolution()
{
	this->problem = new Problem();
	this->result = new Solution();
	this->tempResult = new Solution();
}
/// <summary>
/// Finalizes an instance of the <see cref="InitialSolution"/> class.
/// </summary>
/// <summary>
/// The problem
/// </summary>
/// <returns>Solution *.</returns>
Solution* InitialSolution::NearestNeighbour()
{
	bool flag[MAX_CODINGLENGTH];
	double range = 0;
	double nearCity = 0,nearest = 0;
	double bestObj = 99999999.9f;

	tempResult->Route[0] = 0;
	for(int j=0;j<problem->NCustomer-1;j++)
	{
		int nearCity;
		double nearest = 1000000.0f;
		for(int k = 1; k < this->problem->NCustomer; k++)
		{
			int from = tempResult->Route[j];
			int to = k;
			double range;
			if(flag[k])
			{
				range = this->problem->DistanceMatrix[from][to];
				if (range < nearest && from!=to)
				{
					nearCity = to;
					nearest = range;
				}
			}
		}
		flag[nearCity] = false;
		tempResult->Route[j+1] = nearCity;
	}

	int idx = this->problem->NCustomer;
	for(int i=0;i<this->problem->NDummy;i++)
	{
		tempResult->Route[idx + i] =  0;
	}

	tempResult->Length = this->problem->NCustomer + this->problem->NDummy;
	double currentObj = this->problem->CalculateObjective(tempResult);
	if(currentObj < bestObj)
	{
		result->CopySolution(tempResult);
		bestObj= result->Objective;
	}
	return result;
}