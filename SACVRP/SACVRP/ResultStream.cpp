#include "ResultStream.h"

/// <summary>
/// The _instance{CC2D43FA-BBC4-448A-9D0B-7B57ADF2655C}
/// </summary>
ResultStream* ResultStream::_instance = NULL;

/// <summary>
/// Prevents a default instance of the <see cref="ResultStream"/> class from being created.
/// </summary>
ResultStream::ResultStream(void)
{
}

/// <summary>
/// Finalizes an instance of the <see cref="ResultStream"/> class.
/// </summary>
ResultStream::~ResultStream(void)
{
}

/// <summary>
/// Gets the instance.
/// </summary>
/// <returns>ResultStream *.</returns>
/// / The stream
ResultStream* ResultStream::GetInstance(void)
{
	if(!_instance)
	{
		_instance = new ResultStream();
	}

	return _instance;
}

