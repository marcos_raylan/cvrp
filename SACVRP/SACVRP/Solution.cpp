#include "Solution.h"

/// <summary>
/// Initializes a new instance of the <see cref="Solution"/> class.
/// </summary>
/// / The length
Solution::Solution(void)
{
	this->Length = 0;
	this->Objective = 0.0f;
	this->Feasible = true;
}

/// <summary>
/// Finalizes an instance of the <see cref="Solution"/> class.
/// </summary>
Solution::~Solution(void)
{
}

/// <summary>
/// The route
/// </summary>
/// <param name="">The .</param>
void Solution::CopySolution(Solution* sol)
{
	for(int i = 0; i < sol->Length; i++)
	{
		this->Route[i] = sol->Route[i];
	}
	this->Penalty = sol->Penalty;
	this->Length = sol->Length;
	this->Objective = sol->Objective;
	this->Feasible = sol->Feasible;
}



