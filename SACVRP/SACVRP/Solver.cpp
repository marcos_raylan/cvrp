#include "Solver.h"

/// <summary>
/// Initializes a new instance of the <see cref="Solver" /> class.
/// </summary>
/// / The computation time
Solver::Solver()
{
	this->initialsolution = new InitialSolution();
	this->neighborhood = new Neighborhood();
}
/// <summary>
/// Finalizes an instance of the <see cref="Solver" /> class.
/// </summary>
Solver::~Solver(){}

/// <summary>
/// Initiates the problem.
/// </summary>
/// <param name="">The .</param>
/// / The problem
void Solver::InitiateProblem(Problem* problem)
{
	this->initialsolution->problem = problem;
	this->neighborhood->problem = problem;
	this->problem = problem;
	if (randseed == 0)this->seed = time(NULL);
}
/// <summary>
/// The computation time
/// </summary>
void Solver::Start()
{
	clock_t start;
	srand(seed);
	start=clock();
	this->InitializeSolution();
	problem->CurrentSolution->CopySolution(problem->BestSolution);
	for(int i=0;i<1000;i++)
	{
		problem->WorkingSolution->CopySolution(problem->CurrentSolution);
		for(int j=0;j<100;j++)
		{
			neighborhood->Swap(problem->WorkingSolution);
			problem->WorkingSolution->Objective = problem->CalculateObjective(problem->WorkingSolution);
			if(problem->BestSolution->Objective > problem->WorkingSolution->Objective)
			{
				problem->BestSolution->CopySolution(problem->WorkingSolution);
				problem->CurrentSolution->CopySolution(problem->BestSolution);
			}
		}
	}
	this->ComputationTime = (clock()-start)/(double)CLOCKS_PER_SEC;
}
/// <summary>
/// Initializes the solution.
/// </summary>
void Solver::InitializeSolution()
{
	this->problem->BestSolution = this->initialsolution->NearestNeighbour();
}

