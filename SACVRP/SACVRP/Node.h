// ***********************************************************************
// Assembly         : 
// Author           : Wira Redi
// Created          : 04-01-2015
//
// Last Modified By : Wira Redi
// Last Modified On : 03-30-2015
// ***********************************************************************
// <copyright file="Node.h" company="Global Supply Chain Management NTUST">
//     Copyright (c) Global Supply Chain Management NTUST. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
#pragma once
#ifndef _NODE_
#define _NODE_

/// <summary>
/// Class Node.
/// </summary>
class Node
{
public:
	/// <summary>
	/// The identifier
	/// </summary>
	int ID;
	/// <summary>
	/// The x
	/// </summary>
	float X;
	/// <summary>
	/// The y
	/// </summary>
	float Y;
	/// <summary>
	/// The demand
	/// </summary>
	float Demand;
	Node();
	~Node();
};

#endif