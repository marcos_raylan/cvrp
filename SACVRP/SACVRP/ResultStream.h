// ***********************************************************************
// Assembly         : 
// Author           : Wira Redi
// Created          : 04-01-2015
//
// Last Modified By : Wira Redi
// Last Modified On : 04-21-2014
// ***********************************************************************
// <copyright file="ResultStream.h" company="Global Supply Chain Management NTUST">
//     Copyright (c) Global Supply Chain Management NTUST. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
#pragma once
#ifndef _RESULTSTREAM_
#define _RESULTSTREAM_

#include <iostream>
#include <fstream>

using namespace std;

/// <summary>
/// Class ResultStream.
/// </summary>
class ResultStream
{
private:
	static ResultStream* _instance;
	ResultStream(void);
	~ResultStream(void);
public:
	/// <summary>
	/// The stream
	/// </summary>
	ofstream Stream;
	static ResultStream* GetInstance(void);
};

#endif