// ***********************************************************************
// Assembly         : 
// Author           : Wira Redi
// Created          : 04-01-2015
//
// Last Modified By : Wira Redi
// Last Modified On : 04-26-2014
// ***********************************************************************
// <copyright file="SA.h" company="Global Supply Chain Management NTUST">
//     Copyright (c) Global Supply Chain Management NTUST. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
#include "Solver.h"

#pragma once
#ifndef _SA_
#define _SA_

#define num_generation 3000
#define Alpha 0.999f
#define InitialTemp 15
#define MaxTemp 0.001f
#define NNonImprove 10000000


/// <summary>
/// Class SA.
/// </summary>
class SA : public Solver
{
public:
	/// <summary>
	/// The iteration
	/// </summary>
	float iteration;
	/// <summary>
	/// The alpha
	/// </summary>
	float alpha;
	/// <summary>
	/// The initial temporary
	/// </summary>
	float initialTemp;
	/// <summary>
	/// The maximum temporary
	/// </summary>
	float maxTemp;
	/// <summary>
	/// The non improve
	/// </summary>
	int nonImprove;

	/// <summary>
	/// The temperature
	/// </summary>
	double Temperature;
	/// <summary>
	/// The objective
	/// </summary>
	float Objective;
	/// <summary>
	/// The non improve count
	/// </summary>
	int NonImproveCount;
	SA();
	~SA();
	void Start();
};
#endif