#pragma once
#ifndef _PARAMS_
#define _PARAMS_

#include <time.h>
#include <math.h>
#include <iostream>
#include <string>
#include <stdio.h>

using namespace std;


/* Return a random number between 0 and 1 */
#define getSRand()	((float)rand() / (float)RAND_MAX)

/* Return an integer from 0..(x-1) */
#define getRand(x)	(int)((x) * getSRand())

#define round(x) (float)(floor((x) + 0.5))


#define MAX_CUSTOMER		2000
#define MAX_DEPOSIT			2000
#define MAX_CAR				2000
#define MAX_CODINGLENGTH	MAX_CUSTOMER+MAX_DEPOSIT

#endif