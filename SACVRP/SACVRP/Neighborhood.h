// ***********************************************************************
// Assembly         : 
// Author           : Wira Redi
// Created          : 04-01-2015
//
// Last Modified By : Wira Redi
// Last Modified On : 03-30-2015
// ***********************************************************************
// <copyright file="Neighborhood.h" company="Global Supply Chain Management NTUST">
//     Copyright (c) Global Supply Chain Management NTUST. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
#include "Problem.h"
#pragma once
#ifndef _NEIGHBORHOOD_
#define _NEIGHBORHOOD_

/// <summary>
/// Class Neighborhood.
/// </summary>
class Neighborhood
{
public:
	/// <summary>
	/// The problem
	/// </summary>
	Problem* problem;
	Neighborhood();
	~Neighborhood();
	void Swap(Solution*);
	void Insert(Solution*);
	void Reverse(Solution*);
	void QuickSort(double[],int[],int,int);
};
#endif